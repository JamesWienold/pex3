package fullenigmamachine;

import java.util.LinkedList;
/**
 *
 * @author James Wienold
 */
public class Rotor {

    
    LinkedList<Character> innerRotors = new LinkedList<>();
    LinkedList<Character> outerRotors = new LinkedList<>();
    LinkedList<Character> middleRotors = new LinkedList<>();
    LinkedList<Character> usersMessage = new LinkedList<>();
    LinkedList<Character> testing = new LinkedList<>();

    public Rotor() {
    }
    /**
     *
     * @param innerRotor
     * @param middleRotor
     * @param outerRotor
     * @return
     */
    public String fillRotors(String innerRotor, String middleRotor, String outerRotor) {
        for (char c : innerRotor.toCharArray()) {
            innerRotors.add(c);
        }
        for (char c : middleRotor.toCharArray()) {
            middleRotors.add(c);
        }
        for (char c : outerRotor.toCharArray()) {
            outerRotors.add(c);
        }
        return "We did it";
    }   
    /*
    * A method that does the plugBoard stuff (should work prefectly?)
    */ 
    public char plugBoard(char character, String plugSettings) {
        plugSettings = plugSettings.replace(" ", "");
        plugSettings = plugSettings.toLowerCase();
        System.out.println(plugSettings);
        int position = plugSettings.indexOf(character);
        if (position % 2 == 0) {
            position += 1;
        }
        else {
            position -= 1;
        }
        character = plugSettings.charAt(position);
        System.out.println(character);
        return character;
    }     
    public char rotateEncode(char character) {
        //get the non encrypted character
        int position = innerRotors.indexOf(character);
        Character chr = outerRotors.get(position);
        int middlePosition = middleRotors.indexOf(chr);
        Character encryptedChar = outerRotors.get(middlePosition);
        return encryptedChar;
    }    
    public char rotateDecode(char character) {
        //get the non decrypted character
        int position = outerRotors.indexOf(character);
        Character chr = middleRotors.get(position);
        int outerPosition = outerRotors.indexOf(chr);
        Character decryptedChar = innerRotors.get(outerPosition);
        return decryptedChar;
    }
    /*
    * Rotates the given rotor the given amount times clockwise
    * @param rotor 
    * The rotor that is rotated
    * @param number 
    * How often the rotor is rotated
    */
    public void startingRotation(LinkedList<Character> rotor, int number) {
        for (int n = 0; n < number; n++) {
            rotor.addLast(rotor.poll());
        }
    } 
    /*
    * A mess that needs to be fixed
    */   
    public void encryption(String message, String plugBoardKey) {
        System.out.println(message);
        message = message.replace("[", "");
        message = message.replace("]", "");
        char[] messageAsArray = message.toCharArray();
        for (char c : messageAsArray) {
            char pluggedCharacter = plugBoard(c, plugBoardKey);
            usersMessage.add(pluggedCharacter);
        }
        System.out.println(usersMessage);
        String hello = usersMessage.toString();
        for (char c : hello.toCharArray()) {
            char hey = rotateDecode(c);
            testing.add(hey);
        }
        for (char c : testing) {
            char pluggedCharacter = plugBoard(c, plugBoardKey);
            usersMessage.add(pluggedCharacter);
        }
    }
//    public void setPlugBoard(String[] plugBoard) {
//
//    for (String[] pair : plugBoard) {
//        map.put(pair[0], pair[1]);
//        map.put(pair[1], pair[0]);
//    }
}


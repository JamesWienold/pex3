package fullenigmamachine;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author james
 */
public class EnigmaRun {
    public static void main(String[] args) {
        //EnigmaFrame germansAreSmart = new EnigmaFrame();
        SwingUtilities.invokeLater(() -> new EnigmaFrame());
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {            
        }
    }    
}

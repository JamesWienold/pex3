package fullenigmamachine;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
/**
 *
 * @author James Wienold
 */
public final class EnigmaFrame extends JFrame {

    //Calling rotor class
    private Rotor rotor = new Rotor();
    //Panels
    private JPanel titlePanel;
    private JPanel settingsPanel;
    //GridBagConstraint
    private GridBagConstraints location = new GridBagConstraints();
    //Images
    private ImageIcon enigmaMachinePicture = new ImageIcon(getClass().getResource("../resources/images/enigmaPicture.jpg"));
    //JLabels
    private JLabel enigmaHeader;
    private JLabel enigmaPicture;
    private JLabel innerWheelLabel = new JLabel("Inner Wheel");
    private JLabel middleWheelLabel = new JLabel("Middle Wheel");
    private JLabel outerWheelLabel = new JLabel("Outer Wheel");
    private JLabel codeType = new JLabel("Encode/Decode");
    private JLabel plugboardLabel = new JLabel("Please enter the plugboard settings");
    //JButtons
    private JButton submitButton;
    //JButtonGroups
    final ButtonGroup typeOfCode = new ButtonGroup();
    final ButtonGroup innerWheelInput = new ButtonGroup();
    final ButtonGroup middleWheelInput = new ButtonGroup();
    final ButtonGroup outerWheelInput = new ButtonGroup();
    //JRadioButtons
    private JRadioButton encode = new JRadioButton("Encode");
    private JRadioButton decode = new JRadioButton("Decode");
    private JRadioButton innerWheelCipherOne = new JRadioButton("Cipher One");
    private JRadioButton innerWheelCipherTwo = new JRadioButton("Cipher Two");
    private JRadioButton innerWheelCipherThree = new JRadioButton("Cipher Three");
    private JRadioButton innerWheelCipherFour = new JRadioButton("Cipher Four");
    private JRadioButton innerWheelCipherFive = new JRadioButton("Cipher Five");
    private JRadioButton middleWheelCipherOne = new JRadioButton("Cipher One");
    private JRadioButton middleWheelCipherTwo = new JRadioButton("Cipher Two");
    private JRadioButton middleWheelCipherThree = new JRadioButton("Cipher Three");
    private JRadioButton middleWheelCipherFour = new JRadioButton("Cipher Four");
    private JRadioButton middleWheelCipherFive = new JRadioButton("Cipher Five");
    private JRadioButton outerWheelCipherOne = new JRadioButton("Cipher One");
    private JRadioButton outerWheelCipherTwo = new JRadioButton("Cipher Two");
    private JRadioButton outerWheelCipherThree = new JRadioButton("Cipher Three");
    private JRadioButton outerWheelCipherFour = new JRadioButton("Cipher Four");
    private JRadioButton outerWheelCipherFive = new JRadioButton("Cipher Five");
    //Text Fields
    private JTextField plugboardSettings;
    //Constant
    private Integer[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
        12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27};
    //Scrollers
    private JList innerList;
    private JList middleList;
    private JList outerList;
    private JScrollPane innerStartLocation;
    private JScrollPane middleStartLocation;
    private JScrollPane outerStartLocation;
    //Strings
    private String innerWheelKey;
    private String middleWheelKey;
    private String outerWheelKey;
    private String userMessage;
    private final static String RING_ONE = "GNUAHOVBIPWCJQXDKRY ELSZFMT";
    private final static String RING_TWO = "EJ OTYCHMRWAFKPUZDINSXBGLQV";
    private final static String RING_THREE = "BDFHJLNPRTVXZACEGI KMOQSUWY";
    private final static String RING_FOUR = "KPHDEAC VTWQMYNLXSURZOJFBGI";
    private final static String RING_FIVE = "NDYGLQICVEZRPTAOXWBMJSUHK F";
            
    public EnigmaFrame() {
        super("The Enigma Machine");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900, 600);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());
        //Title Panel, adding the picture and label.
        titlePanel();
        //List for scrolling
        //Setting the list to the scroll
        scrollingList();
        //The plugboard input
        plugboardSettings = new JTextField(26);
        //JButton for submission. Calling actionListener.
        submitButton = new JButton("SUBMIT");
        ButtonHandler handler = new ButtonHandler();
        submitButton.addActionListener(handler);
        //Adding JButton's to the right Groups
        addComponentsToButtonGroups();
        settingsPanel = new JPanel();
        settingsPanel.setLayout(new GridBagLayout());
        setFocusability();
        setFrameLocation();
        setVisible(true);
    }
    /**
     * Creates the titlePanel, which is the header
     */
    public void titlePanel() {
        titlePanel = new JPanel();
        enigmaHeader = new JLabel("The Best Enigma Machine in Town");
        enigmaPicture = new JLabel(enigmaMachinePicture);
        titlePanel.add(enigmaPicture);
        titlePanel.add(enigmaHeader);
    }
    /**
     *Creates the three scrolling list on the JFrame
     */
    public void scrollingList() {
        innerList = new JList(data);
        innerList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        innerList.setLayoutOrientation(JList.VERTICAL);
        innerList.setVisibleRowCount(-1);
        middleList = new JList(data);
        middleList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        middleList.setLayoutOrientation(JList.VERTICAL);
        middleList.setVisibleRowCount(-1);
        outerList = new JList(data);
        outerList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        outerList.setLayoutOrientation(JList.VERTICAL);
        outerList.setVisibleRowCount(-1);
        innerStartLocation = new JScrollPane(innerList);
        innerStartLocation.setPreferredSize(new Dimension(50, 80));
        middleStartLocation = new JScrollPane(middleList);
        middleStartLocation.setPreferredSize(new Dimension(50, 80));
        outerStartLocation = new JScrollPane(outerList);
        outerStartLocation.setPreferredSize(new Dimension(50, 80));
    }
    /**
     * A method to add all the components to the button groups (clean up the constructor)
     */
    public void addComponentsToButtonGroups() {
        typeOfCode.add(encode);
        typeOfCode.add(decode);
        innerWheelInput.add(innerWheelCipherOne);
        innerWheelInput.add(innerWheelCipherTwo);
        innerWheelInput.add(innerWheelCipherThree);
        innerWheelInput.add(innerWheelCipherFour);
        innerWheelInput.add(innerWheelCipherFive);
        middleWheelInput.add(middleWheelCipherOne);
        middleWheelInput.add(middleWheelCipherTwo);
        middleWheelInput.add(middleWheelCipherThree);
        middleWheelInput.add(middleWheelCipherFour);
        middleWheelInput.add(middleWheelCipherFive);
        outerWheelInput.add(outerWheelCipherOne);
        outerWheelInput.add(outerWheelCipherTwo);
        outerWheelInput.add(outerWheelCipherThree);
        outerWheelInput.add(outerWheelCipherFour);
        outerWheelInput.add(outerWheelCipherFive);
    }
    /**
     * A method to set all the focusable to false (clean up the constructor)
     */
    public void setFocusability() {
        innerWheelCipherOne.setFocusable(false);
        innerWheelCipherTwo.setFocusable(false);
        innerWheelCipherThree.setFocusable(false);
        innerWheelCipherFour.setFocusable(false);
        innerWheelCipherFive.setFocusable(false);
        middleWheelCipherOne.setFocusable(false);
        middleWheelCipherTwo.setFocusable(false);
        middleWheelCipherThree.setFocusable(false);
        middleWheelCipherFour.setFocusable(false);
        middleWheelCipherFive.setFocusable(false);
        outerWheelCipherOne.setFocusable(false);
        outerWheelCipherTwo.setFocusable(false);
        outerWheelCipherThree.setFocusable(false);
        outerWheelCipherFour.setFocusable(false);
        outerWheelCipherFive.setFocusable(false);
        encode.setFocusable(false);
        decode.setFocusable(false);
    }   
    /**
     * A method that uses GridBagLayouts to set the location of all the JComponents
     */
    public void setFrameLocation() {
        location.fill = GridBagConstraints.VERTICAL;
        location.weightx = 0.5;
        location.gridx = 0;
        location.gridy = 0;
        settingsPanel.add(codeType, location);
        location.gridy++;
        settingsPanel.add(encode, location);
        location.gridy++;
        settingsPanel.add(decode, location);
        location.weightx = 1.0;
        location.gridx = 1;
        location.gridy = 0;
        settingsPanel.add(innerWheelLabel, location);
        location.gridy++;
        settingsPanel.add(innerWheelCipherOne, location);
        location.gridy++;
        settingsPanel.add(innerWheelCipherTwo, location);
        location.gridy++;
        settingsPanel.add(innerWheelCipherThree, location);
        location.gridy++;
        settingsPanel.add(innerWheelCipherFour, location);
        location.gridy++;
        settingsPanel.add(innerWheelCipherFive, location);
        location.gridy++;
        settingsPanel.add(innerStartLocation, location);
        location.weightx = 1.0;
        location.gridx = 3;
        location.gridy = 0;
        settingsPanel.add(outerWheelLabel, location);
        location.gridy++;
        settingsPanel.add(outerWheelCipherOne, location);
        location.gridy++;
        settingsPanel.add(outerWheelCipherTwo, location);
        location.gridy++;
        settingsPanel.add(outerWheelCipherThree, location);
        location.gridy++;
        settingsPanel.add(outerWheelCipherFour, location);
        location.gridy++;
        settingsPanel.add(outerWheelCipherFive, location);
        location.gridy++;
        settingsPanel.add(outerStartLocation, location);
        location.weightx = 1.0;
        location.gridx = 2;
        location.gridy = 0;
        settingsPanel.add(middleWheelLabel, location);
        location.gridy++;
        settingsPanel.add(middleWheelCipherOne, location);
        location.gridy++;
        settingsPanel.add(middleWheelCipherTwo, location);
        location.gridy++;
        settingsPanel.add(middleWheelCipherThree, location);
        location.gridy++;
        settingsPanel.add(middleWheelCipherFour, location);
        location.gridy++;
        settingsPanel.add(middleWheelCipherFive, location);
        location.gridy++;
        settingsPanel.add(middleStartLocation, location);
        location.gridx = 4;
        location.gridy = 0;
        settingsPanel.add(plugboardSettings, location);
        location.gridy++;
        settingsPanel.add(plugboardLabel, location);
        location.ipady = 10;
        location.gridx = 3;
        location.gridy = 10;
        settingsPanel.add(submitButton, location);
        location.ipady = 0;
        location.weighty = 0;
        location.gridx = 0;
        location.gridy = 0;
        add(titlePanel, location);
        location.gridx = 0;
        location.gridy = 1;
        location.weighty = 1.0;
        location.gridheight = 500;
        add(settingsPanel, location);
    }    
    /**
     * ButtonHandler that is called after submit is pushed
     */
    private class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String selectedInner = innerList.getSelectedValue().toString();
            String selectedMiddle = middleList.getSelectedValue().toString();
            String selectedOuter = outerList.getSelectedValue().toString();
            String plugboardKey = plugboardSettings.getText();
            System.out.println(selectedInner + selectedMiddle + selectedOuter + plugboardKey);
            try {
                userMessage = FileExplorer.getTextFile();
            } catch (IOException ex) {
                Logger.getLogger(EnigmaFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(encode.isSelected()) {
                
            }
            else if(decode.isSelected()) {
                
            }
            //Inner wheel selection
            if(innerWheelCipherOne.isSelected()) {
                innerWheelKey = RING_ONE;
            }
            else if(innerWheelCipherTwo.isSelected()) {
                innerWheelKey = RING_TWO;
            }
            else if(innerWheelCipherThree.isSelected()) {
                innerWheelKey = RING_THREE;
            }
            else if(innerWheelCipherFour.isSelected()) {
                innerWheelKey = RING_FOUR;
            }
            else if(innerWheelCipherFive.isSelected()) {
                innerWheelKey = RING_FIVE;
            }
            //Middle wheel selection
            if(middleWheelCipherOne.isSelected()) {
                middleWheelKey = RING_ONE;
            }
            else if(middleWheelCipherTwo.isSelected()) {
                middleWheelKey = RING_TWO;
            }
            else if(middleWheelCipherThree.isSelected()) {
                middleWheelKey = RING_THREE;
            }
            else if(middleWheelCipherFour.isSelected()) {
                middleWheelKey = RING_FOUR;
            }
            else if(middleWheelCipherFive.isSelected()) {
                middleWheelKey = RING_FIVE;
            }
            //Outer wheel selection
            if(outerWheelCipherOne.isSelected()) {
                outerWheelKey = RING_ONE;
            }
            else if(outerWheelCipherTwo.isSelected()) {
                outerWheelKey = RING_TWO;
            }
            else if(outerWheelCipherThree.isSelected()) {
                outerWheelKey = RING_THREE;
            }
            else if(outerWheelCipherFour.isSelected()) {
                outerWheelKey = RING_FOUR;
            }
            else if(outerWheelCipherFive.isSelected()) {
                outerWheelKey = RING_FIVE;
            }
            System.out.println(innerWheelKey + " " + middleWheelKey + " " + outerWheelKey);
            rotor.fillRotors(innerWheelKey, middleWheelKey, outerWheelKey);
            rotor.encryption(userMessage, plugboardKey);
        }
    }
}
